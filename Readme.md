# ScreenCloud ATM

## Brief

This was a code challenge for a potential job with ScreenCloud as a Front End Web Developer.
At the time of applying, I had zero experience in React and so I designed it in jQuery. Following
on from the interview, I decided to try and build it in React. 

## Update: 20/11/19

Today, I uploaded the last parts of the React ATM app. This was a massive learning experience for me,
and I had a real blast building and learning it.

## Technologies Used

Languages used: React.js, jQuery, JS, CSS, HTML

IDE: Notepad++

**William McCulla**