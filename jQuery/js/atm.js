var customerBalance = 0;
var pinCounter = 0;
var pinError = 0;
var todayDate = new Date();
var currentHour = todayDate.getHours();
var sessionIniatiated = false;
var overdrawn = false;
var withdrawAmount = 0;
var total20 = 7;
var total10 = 15;
var total5 = 4;	

$(document).ready(function(){
	// Intro Card Click
	$("#creditCard").click(function(){
		// Animates card going into slot
		$(this).animate({
			left: "200%" 
			}, 750, function() {		
				// If user has locked account by entering pin 3 times, presents error otherwise continues with app
				if (pinError == 3) {
						$("#introCont").fadeOut(500, function() {
							$("#pinLocked").fadeIn(500);
							$("#header").fadeIn();
						});
				}
				else {
					$("#introCont").fadeOut(500, function() {
						$("#pinCont").fadeIn();
						$("#header").fadeIn();
					});
				}
			});
	});

	// When User clicks to cancel full transaction, resets app to start minus session values	
	$("#cancelButton").click(function() {
		$("#header").fadeOut(400);
		$("[activeCont]").fadeOut(500, function() {
			$("#introCont").fadeIn(500);
			$("#creditCard").css("left", "50%");
		});		
		pinReset();
	});	
		
	// When User clicks a number on PIN keypad, adds value to PIN value + display
	$(".pKeypad").click(function() {
		if (pinCounter < 4) {
			var curNo = $(this).attr("value");
			
			$("#pinEntry").attr("value", $("#pinEntry").attr("value") + curNo);
			$("#pinEntry").append("*");
			pinCounter++;
		}
	});
		
	// When User clicks a number on withdrawal keypad, adds value to withdrawal value + display	
	$(".wKeypad").click(function() {
		var curNo = $(this).attr("value");
			
		$("#wEntry").attr("value", $("#wEntry").attr("value") + curNo);
		$("#wEntry").append(curNo);
	});
		
		
	// When user submits pin	
	$("#pinSubmit").click(function(){
		var pin = $("#pinEntry").attr("value");
		
		// User must have entered pin within 3 attempts
		if (pinError < 3) {
			// Runs ajax call to API to verify PIN and get balance
			$.ajax({
				url:"https://frontend-challenge.screencloud-michael.now.sh/api/pin/",
				type:"POST",
				data: JSON.stringify({ "pin" : pin }),
				contentType:"application/json",
				success: function(data){
					
				// If user has not used app in this session, updates balance from API amount
					if (sessionIniatiated == false) {
						customerBalance = data.currentBalance;						
						sessionIniatiated = true;
					}
					// Reset PIN attempts
					pinError = 0;
					$("#pinCont").fadeOut(500, function() {
						updateBalanceDisp();
					});
					
					// Resets PIN keypad
					pinReset();
				},
				error: function() {
					// If user enters the wrong PIN, reset keypad and add one attempt to counter
					pinReset();
					$('#pinHeader').html("Incorrect PIN<br>Please try again");
					pinError++;
					
					// Three wrong PIN attempts means the user is locked out from their account
					if (pinError == 3)
					{
						$("#pinCont").fadeOut(500, function() {
							$("#pinLocked").fadeIn(500);
						});
					}
				}
			})
		}
	});
		
	// Reset PIN Keypad	
	function pinReset() {
		$('#pinHeader').html("Enter your PIN");
		$("#pinEntry").attr("value", "");
		$("#pinEntry").html("&nbsp;");
		pinCounter = 0;
	}
		
	// Small touch of personalisation; user will get a greeting based on time of day
	if (currentHour < 12) {
		$("#timeOfDay").html("morning");
	} 
	else if (currentHour < 18) {
		$("#timeOfDay").html("afternoon");
	} 
	else {
		$("#timeOfDay").html("evening");
	}
		
	// Event handler for when user presses one of the preset withdrawal amount buttons	
	$(".balButton").click(function() {

		// If ATM has determined it doesn't have enough notes to withdraw preset amount, becomes inactive
		if ($(this).hasClass('inactiveButton') == false)
		{
			// If user presses other button, presents customer withdrawal amount
			if ($(this).attr('value') == 'other')
			{
				$('#balanceCont').fadeOut(500);
				$('#withdrawalEntry').fadeIn(500);
			}
			else
			{	
				// Other wise check if user has enough to withdraw and continue
				checkBalanceWithdraw($(this).attr("value"));
			}
		}
	});
		
	// Action carried out on withdrawal
	function withdraw(withdrawAmm) {		

		var withdrawnTotal = withdrawAmm;
		var counter20 = total20;
		var counter10 = total10;
		var counter5 = total5;
		var notesArray = [];
			
		$("#moneyCont").empty();
			
		// ATM will continue to withdraw money until it runs out of notes
		while (withdrawnTotal != 0) {	
			// If remaining amount is multiple of 20 etc. withdraw that note if there is one to withdraw
			// Continues down through demoninations until no notes left or full balance withdrawn
			if((withdrawnTotal % 20) == 0 && counter20 != 0) {		
				withdrawnTotal -= 20;
				counter20--;
				notesArray.push(20);
			}
			else if ((withdrawnTotal % 10) == 0 && counter10 != 0) {			
				withdrawnTotal -= 10;		
				counter10--;
				notesArray.push(10);
			}
			else if ((withdrawnTotal % 5) == 0 && counter5 != 0) {		
				withdrawnTotal -= 5;		
				counter5--;	
				notesArray.push(5);				
			}
			else if (counter5 == 0 && counter10 == 0 && counter20 == 0) {
				break;
			}
		}
			
		// If the withdrawal is complete, deduct cash from balance and notes from counter
		if (withdrawnTotal == 0)
		{
			customerBalance -= withdrawAmm;
			total20 = counter20;
			total10 = counter10;
			total5 = counter5;
			
			// Arranges notes from highest to lowest, then draws them on DOM
			notesArray.sort(function(a, b){return b-a});
			notesArray.forEach(drawMoney);
			
			// Arranges drawn notes in fan shape
			var notesArray = $(".note").toArray();
			var angleDif = 180 / (notesArray.length + 1);
			var angle = angleDif - 90;
				
			for (i = 0; i < notesArray.length; i++) {			
				$(notesArray[i]).css({
				'transform' : 'translate(-50%, -50%) rotate('+ angle +'deg)'});
				angle += angleDif;
			}
			
			// Display withdrawal confirmation screen
			$('#withdrawConfirmAmm').html(withdrawAmm);
			$("[activeCont]").fadeOut(500);
			$('#withdrawalSuccess').fadeIn(500);
			}
		else {
			// Display not enough notes error message
			$("[activeCont]").fadeOut(500);
			$('#notEnoughNotes').fadeIn(500);
		}
	}

	// Update balance display
	function updateBalanceDisp() {
		$("#balanceDisp").html(customerBalance);
		$("#maximumWithdraw").html((customerBalance + 100));
		$(".wHeader").html("Enter how much you'd like to withdraw");
		resetWithdraw();
		
		var balButtonsArray = $(".balButton").toArray();

		// Check to see if preset withdrawal buttons are still valid with notes in machine
		for (i = 0; i < (balButtonsArray.length - 1); i++) {
			var buttonValue = Number(balButtonsArray[i].value);
			var counter20 = total20;
			var counter10 = total10;
			var counter5 = total5;

			while (buttonValue != 0) {	
				if((buttonValue % 20) == 0 && counter20 != 0) {		
					buttonValue -= 20;
					counter20--;
				}
				else if ((buttonValue % 10) == 0 && counter10 != 0) {			
					buttonValue -= 10;
					counter10--;
				}
				else if ((buttonValue % 5) == 0 && counter5 != 0) {		
					buttonValue -= 5;
					counter5--;			
				}
				else {
					balButtonsArray[i].classList.add("inactiveButton");
					break;
				}
			}
		}
			
		// If no notes left, display error message
		if (total20 == 0 && total10 == 0 && total5 == 0)
		{
			$("[activeCont]").fadeOut(500);
			$("#header").fadeOut(500);
			$('#outofMoneyWarning').fadeIn(500);
		}
		else {
			$("#balanceCont").fadeIn(500);
		}
	}

	// Draws money imgs to DOM
	function drawMoney(item) {	
		$("#moneyCont").append("<img src='img/"+ item +"pound.png' class='note'>");		
	}
		
	// Updates balance screen then returns user to it
	$('#returnToBalance').click(function() {
		$('#withdrawalSuccess').fadeOut(500);
		updateBalanceDisp();
	});
		
	// Check custom withdrawal amount is valid and machine has enough notes to give out
	$("#withdrawButton").click(function(){	
		var withdrawAmount = Number($("#wEntry").attr('value'));
			
		if (!isNaN(withdrawAmount)) {
			if ((withdrawAmount % 5) == 0) {
				if (total5 != 0) {
					checkBalanceWithdraw(withdrawAmount);
				}
				else if ((withdrawAmount % 10) == 0) {
					if (total10 != 0) {
						checkBalanceWithdraw(withdrawAmount);
					}
					else if ((withdrawAmount % 20) == 0) {
						if (total20 != 0) {
							checkBalanceWithdraw(withdrawAmount);
						}
					}
					else {
						$('.wHeader').html('No &pound;10 notes left: Please enter a multiple of 20.');
						resetWithdraw();
					}
				}
				else {
					$('.wHeader').html('No &pound;5 notes left: Please enter a multiple of 10.');
					resetWithdraw();
				}
			}
			else {
				$('.wHeader').html('Invalid amount: Please enter a multiple of 5.');
				resetWithdraw();
			}
		}
		else {
			$('.wHeader').html('Please enter a valid number.');
			resetWithdraw();
		}
	});
		

	// Resets withdrawal keypad
	function resetWithdraw() {
		$('#wEntry').attr('value', '');
		$('#wEntry').html('&pound;');
	}
		
	// Check Customer's Withdrawal against Balance to see if Withdrawal is possible
	function checkBalanceWithdraw(withdrawAmm) {
		if ((customerBalance - withdrawAmm) >= 0) {
			// If user is not going to go into overdraft
			withdraw(withdrawAmm);
		}
		else if ((customerBalance - withdrawAmm) >= -100 && overdrawn == false)
		{
			// If user is going into overdraft and hasn't acknowledged warning
			$("[activeCont]").fadeOut(500);
			$('#overdraftWarning').fadeIn(500);
			withdrawAmount = withdrawAmm;
		}
		else if ((customerBalance - withdrawAmm) >= -100 && overdrawn == true) {
			// If user is going into overdraft and has acknowledged warning
			withdraw(withdrawAmm);
		}
		else {
			// If user has insuffient funds
			$("[activeCont]").fadeOut(500);
			$('#insufficientFunds').fadeIn(500);
		}
	}
		
	$('.overdraftButton').click(function () {
		
		// Returns user to balance screen without a withdrawal
		if ($(this).attr('value') == "no")
		{
			$("[activeCont]").fadeOut(500);
			updateBalanceDisp();
		}
		else 
		{
			// Continues with withdrawal
			withdraw(withdrawAmount);
			overdrawn = true;
		}
	});

	// Allows user to clear entered balance
	$('#clearButton').click(function() {
		resetWithdraw();
	});
});	