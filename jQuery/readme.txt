Super Awesome Bank (SAB) - Automated Teller Machine

CONTENTS
1. Installation
2. Features
3. Limitations
4. Technologies Used

1. Installation

No further set up is required, all that is needed to run the solution is that the file 'atm.html' 
is opened in a web browser (as long as it has the same files in the same subdirectories as the initial
download.

2. Features

The original project specification only outlined that the user must enter a PIN and then be able to withdraw
custom amounts of money, the machine must detect levels of this money within the machine and act accordingly.

With the above in mind, the finished solution is a full GUI with a high UX in mind. To that end, the app is
responsive to most screen sizes and orientations.

Instead of just allowing custom amounts, the solution also provides users with preset amounts (which are 
invalidated when the machine cannot process the withdrawal). It then displays the money in a graphic format 
rather than just a text outlay of which notes have been withdrawn. 

The solution is personalised to both the user and the time of day. It warns users if they are going to go overdrawn,
and then does the sensible thing and doesn't let users go into an unauthorised overdraft, saving much heartache. 
If the machine runs out of money, it does that heavily annoying thing when you're searching for an ATM on a night out 
and you finally find one, run to it in the pouring rain, only to be greeted with 'Out of Service'...only 
this time, the SAB apologises for it!

3. Limitations

Due to this being a coding challenge, and not a full fledged app, a number of corners have been cut to facilitate
development. This is highly evident in that there is no way to update the remote balance so that the withdrawals are
permanent. Because of this, I have left the solution in such a way that the notes held in the machine are not permanently
held. This could be changed by having access to a database and conducting update AJAX calls to the database as well as holding
a local storage for the notes for the machine. 

As well, given the sensitive nature of banking transactions, there would normally be a large amount of encryption and extra
data security. However, the API does not allow for this, therefore my solution also does not include this. Another limitation is
limited information provided by the API. For example, my solution mentions the user's name. This could be made dynamic if the API
provided a name.

As well, a glaring limitation is that only one user can actually access the app. This could be circumnavigated by asking a user to
provide an account number or by card reader. 

4. Technologies Used

Languages used: HTML, CSS, JS, jQuery
IDE: Notepad++
Icons courtesy of Icomoon.io
Notes images royalty free from Shutterstock