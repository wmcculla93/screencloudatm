import React from 'react';
import './Header.css';
import cancel from './assets/cancel.svg';

class Header extends React.Component { 

  render() {  
    return (
      <div className="Header-container">
	    <table className="Header-table">
		  <tbody>
		    <tr>
			  <td className="Header-table-left">
			    <button className="Header-cancel-button" onClick={() => this.props.resetApp(0, 0, true)}>
				  <img src={cancel} className="Header-logo" alt="cancel" /> Cancel
				</button>
			  </td>
			  <td className="Header-table-right">
			    SAB
			  </td>
			</tr>
		  </tbody>
		</table>
	  </div>
    )
  }
}

export default Header;