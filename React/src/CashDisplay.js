import React from 'react';
import Note from './Note';
import './CashDisplay.css';

/* Screen which graphically shows money withdrawal */

class CashDisplay extends React.Component { 

  constructor() {
    super();
    this.state = {
	  fadeTransition: 0
	};
  }
  
  // When component mounts, fade in
  componentDidMount() {	
	let fadeIn = setInterval(() => {
	  this.setState({
	    fadeTransition: 1
	  })
	  clearInterval(fadeIn)
	}, 10)
  }
  
  // Builds array of money notes and rotates accordingly
  drawMoney() {
	let angleDifference = 180 / (this.props.notes.length + 1),
	angle = angleDifference - 90,
	cashArray = []
	  
	for (let i = 0; i < this.props.notes.length; i++)
	{
	  cashArray.push(<Note key={i} type={this.props.notes[i]} angle={angle} />)
	  angle += angleDifference
	}
	
	return cashArray
  }

  render() {
    return(
	  <div className="mainContainer" style={{'opacity' : this.state.fadeTransition}}>
	    <h1>
		  Ch-ching!
		</h1>
	    <div className="CashDisplay-money-container">
	      {this.drawMoney()}
		</div>
		<h3>
		  You withdrew £{this.props.withdrawAmm}.00!
		</h3>
	    <button onClick={() => this.props.changeScreen(2)} className="button">
	      Return
	    </button>
	  </div>
	)
  }
}

export default CashDisplay;