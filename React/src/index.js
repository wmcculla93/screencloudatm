import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ATM from './ATM';

ReactDOM.render(<ATM />, document.getElementById('root'));
