import React from 'react';
import './Note.css';
import five from './assets/5pound.png';
import ten from './assets/10pound.png';
import twenty from './assets/20pound.png';

/* Note graphic when money is rendered */
class Note extends React.Component {  

  // Determine which note is to be displayed
  whichNote = () => {
	switch(this.props.type) {
	  case 5:
	    return five
	  case 10:
	    return ten
	  case 20:
	    return twenty
	  default:
	    return null
	}
  }

  render() {
    return(
      <img className="Note-img" alt="" src={this.whichNote()}
	  style={{'transform' : 'translate(-50%, -50%) rotate('+ this.props.angle +'deg)'}}/>
	)
  }
}

export default Note;