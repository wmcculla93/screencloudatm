import React from 'react';
import './Intro.css';
import credit from './assets/credit.svg';

/* Landing page when user opens or resets app */
class Intro extends React.Component {
	
  constructor() {
    super();
    this.state = {
	  cardClicked: false,
	  fadeTransition: 0
	}
  }
  
  // When component mounds, fade in
  componentDidMount() {	
	let fadeIn = setInterval(() => {
	  this.setState({
	    fadeTransition: 1
	  })
	  clearInterval(fadeIn)
	}, 10)
  }
  
  // Handle when user clicks card
  cardClick = () => {
    this.setState({
	  cardClicked: true,
	  fadeTransition: 0
	})
	this.props.changeScreen(1, 0, false)
  }
	
  render() {
	  
	  // Set card position for animation
	  let cardLeft = "0" 
	  if (this.state.cardClicked) {
		cardLeft = "150%"
	  }
	  
	  return (
      <div className="mainContainer" style={{'opacity' : this.state.fadeTransition}}>
        <h1 className="Intro-bigText">SAB</h1>
        <p>
          Welcome to Super Awesome Bank!
        </p>
		<p>
		  Insert your card to begin
		</p>
		<div className="Intro-cardSlotWrapper">
		  <div className="Intro-cardWrapper">
		    <img src={credit} style={{'left' : cardLeft}} className="Intro-card" 
		    alt="Please insert to begin" onClick={this.cardClick} />
		  </div>
		  <div className="Intro-cardSlot">
		  </div>
	    </div>
      </div>
	)
  }
}

export default Intro;
