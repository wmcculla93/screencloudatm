import React from 'react';

/* Overdraft warning if user is about to go overdrawn */
class Overdrawn extends React.Component { 

  constructor() {
    super();
	
    this.state = {
	  fadeTransition: 0
	};
  }
  
  // When component is mounted, fade in
  componentDidMount() {	  
	let fadeIn = setInterval(() => {
	  this.setState({
	    fadeTransition: 1
	  })
	  clearInterval(fadeIn)
	}, 10)
  }

  render() {  
    return (
      <div className="mainContainer" style={{'opacity' : this.state.fadeTransition}}>
		<div>
		  This transaction will cause your account to go into overdraft, do you want
		  to proceed?
		</div>
		<br />
		<table>
		  <tbody>
		    <tr>
			  <td>
		        <button className="button sideBySide" onClick={() => this.props.withdraw(this.props.withdrawAmm)}>
				  Yes
        		</button>
			  </td>
		    </tr>
			<tr>
			  <td>
		        <button className="button sideBySide" onClick={() => this.props.changeScreen(2)}>
		          No
                </button>
			  </td>
		    </tr>
		  </tbody>
		</table>
      </div>
    );
  }
}

export default Overdrawn;