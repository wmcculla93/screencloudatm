import React from 'react';
import './Balance.css';
import KeypadButton from './KeypadButton';

/* Screen which displays the user's balance and withdrawal options */

class Balance extends React.Component {
  
  constructor() {
    super();
    this.state = {
	  fadeTransition: 0,
	};
  }
  
  
  // When component is mounted, fade in
  componentDidMount() {	
	let fadeIn = setInterval(() => {
	  this.setState({
	    fadeTransition: 1
	  })
	  clearInterval(fadeIn)
	}, 10)
  }
  
  // Touch of personalisation, gives users greeting
  getTime = () => {
	let todayDate = new Date(),
    currentHour = todayDate.getHours()
	
    if (currentHour < 12) {
      return "morning"
    } 
    else if (currentHour < 18) {
      return "afternoon"
    } 
    else {
	  return "evening"
    }
  }
  
  // If user clicks button, either run withdrawal or send to screen for custom withdrawal
  balanceClick = (i) => {
	if (!isNaN(i)) {
	  this.props.withdraw(i)
	}
	else {
	  this.props.changeScreen(4)
	}
  }
  
  // Builds table of buttons based on array
  renderBalanceButtons = () => {
	const buttonValues = ["5", "40", "10", "50", "20", "100", "30", "Other"]	
	let buttons = [],
	buttonCounter = 0
	
	for (let i = 0; i < (buttonValues.length / 2); i++) {	
      let buttonRow = []
      for (let j = 0; j < 2; j++) {
        
		let buttonLabel = buttonValues[buttonCounter]		
		if (!isNaN(buttonLabel)) {
		  buttonLabel = "£".concat(buttonLabel.toString())
		}		  
	  
	    buttonRow.push(<td className="Balance-cell" key={buttonCounter}><KeypadButton classType="KeypadButton-balance button" 
		buttonValue={buttonValues[buttonCounter]} buttonLabel={buttonLabel} buttonClick={this.balanceClick} /></td>)
		
		buttonCounter++	
		if (buttonCounter === buttonValues.length) {
		  break
		}
	  }
	  buttons.push(<tr key={i}>{buttonRow}</tr>)
	}
	return buttons
  }
  
  render() {	  
	  return (
      <div className="mainContainer" style={{'opacity' : this.state.fadeTransition}}>
        <table className="Balance-table Balance-topTable">
		  <tbody>
		    <tr className="medium-text">
			  <td rowSpan="2" className="Balance-leftColumn">
			    Good {this.getTime()}, Michael
			  </td>
			  <td className="Balance-rightColumn Balance-bold">
			    &pound;{this.props.balance}.00
			  </td>
		    </tr>
		    <tr className="small-text">
			  <td colSpan="2" className="Balance-rightColumn">
			    Balance
		      </td>
		    </tr>
		  </tbody>  
	    </table>
		<br />
		<table className="Balance-table">
		  <tbody>
		    {this.renderBalanceButtons()}
		  </tbody>
		</table>
      </div>
	)
  }
}

export default Balance;