import React from 'react';
import './PIN.css';
import Keypad from './Keypad';

/* Screen for user to enter PIN on app start up */
class PIN extends React.Component { 

  constructor() {
    super();
    this.state = {
	  pinEntry: "",
	  pinError: false,
	  pinValue: "",
	  pinCounter: 0,
	  fadeTransition: 0,
	  pinErrorCounter: 0
	};
  }
  
  // When component is mounted, fade in
  componentDidMount() {	
	let fadeIn = setInterval(() => {
	  this.setState({
	    fadeTransition: 1
	  })
	  clearInterval(fadeIn)
	}, 10)
  }
  
  // When user clicks keypad button, update PIN value, * display and counter so no more than 4 digits can be entered
  keypadClick = (i) => {  
	if (this.state.pinCounter !== 4) {
      this.setState({
		pinEntry: (this.state.pinEntry + "*"),
		pinValue: this.state.pinValue + i,
		pinCounter: (this.state.pinCounter + 1)
	  })
	}	  
  }
  
  // When user submits PIN, send AJAX call to API with data
  submitPin = () => {
    fetch('https://frontend-challenge.screencloud-michael.now.sh/api/pin/', {
	  method: 'post',
	  headers: {
	    'Content-Type': 'application/json'
	  },
	  body: JSON.stringify({ "pin" : this.state.pinValue})
	}).then(function (response) {	  
	  return response.json()
	}).then((data) => {
	  if (typeof data.currentBalance !== "undefined") {
		// If call is returned and successful, update user balance, fade out screen and show balance
        this.props.updateBalance(data.currentBalance)		  
		this.setState({
	      fadeTransition: 0
	    })
	    this.props.changeScreen(2)
	  }
	  else {
		// If failed, reset pin entry, display error message
		this.setState({
		  pinEntry: "",
	      pinError: true,
	      pinValue: "",
	      pinCounter: 0,
		  pinErrorCounter: (this.state.pinErrorCounter + 1)
	    })
        
		// If failed three times, lock user out of app
        if (this.state.pinErrorCounter === 3) {
          this.props.changeScreen(6, 4, true)
		}		
	  }
	})
  }
	
  render() {

    // Determine error message
    let pinErrorMsg = "Please enter your PIN"
	if (this.state.pinError) {
	  pinErrorMsg = "Incorrect PIN"
	}
  
    return (
      <div className="mainContainer" style={{'opacity' : this.state.fadeTransition}}>
        <p>
		  {pinErrorMsg}
		</p>
		<div className="PIN-entry">
		  {this.state.pinEntry}
		</div>
        <div>
		  <Keypad keypadClick={this.keypadClick} />
		</div>
		<button className="button" onClick={this.submitPin}>
		  Submit
		</button>
      </div>
    );
  }
}

export default PIN;
