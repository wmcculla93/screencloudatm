import React from 'react';

/* Error display for user */
class ErrorScreen extends React.Component { 

  constructor() {
    super();
	
    this.state = {
	  fadeTransition: 0
	};
  }
  
  componentDidMount() {	
    // If error is non-escapable, hide the return button
    let submitFlag = false
	
	if (this.props.error === 2 || this.props.error === 4) {
	  submitFlag = true
	}
    
	// When component mounts, fade in screen
	let fadeIn = setInterval(() => {
	  this.setState({
	    fadeTransition: 1,
	    hideSubmit: submitFlag
	  })
	  clearInterval(fadeIn)
	}, 10)
  }
  
  // Set error message
  displayError = () => {
	switch(this.props.error) {
	  case 0:
	    return "There are not enough notes in this machine for this transaction. Please enter a lower amount."
	  case 1:
	    return "You cannot withdraw this amount due to insufficient funds in your account."
	  case 2:
	    return "There are no notes left in this machine. Please try again later."
	  case 3:
	    return "Please enter a multiple of 5 to withdraw."
	  case 4:
	    return "Your account has been locked. Please contact our customer service team as soon as possible."
	  default:
	    return null
	}
  }

  render() {  
    return (
      <div className="mainContainer" style={{'opacity' : this.state.fadeTransition}}>
		<div>
		{this.displayError()}
		</div>
		<br />
		{!this.state.hideSubmit ? 
		  <button className="button" onClick={() => this.props.changeScreen(2)}>
		    Return
          </button> : null}
      </div>
    );
  }
}

export default ErrorScreen;