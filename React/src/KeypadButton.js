import React from 'react';
import './KeypadButton.css';

/* Individual button for keypad */
class KeypadButton extends React.Component { 

  render() {	  
    return (
	  <button className={this.props.classType} onClick={() => this.props.buttonClick(this.props.buttonValue)}>
	    {this.props.buttonLabel}
      </button>
	)
  }
}

export default KeypadButton;