import React from 'react';
import Header from './Header';
import Intro from './Intro';
import PIN from './PIN';
import Balance from './Balance';
import CashDisplay from './CashDisplay';
import CustomBalance from './CustomBalance';
import Overdrawn from './Overdrawn';
import ErrorScreen from './ErrorScreen';

/* General container for app */
class ATM extends React.Component {
	
  constructor() {
    super();
    this.state = {
	  currentScreen: 0,
	  currentBalance: 0,
	  balanceSet: false,
	  total20: 7,
	  total10: 15,
	  total5: 4,
	  notesArray: [],
	  overdrawnFlag: false,
	  headerHide: true
	}
  }
  
  // If app is navigating to different screen, fade out current and display new
  changeScreen = (screen, error, header) => {	  
	let cardAnimation = setInterval(() => {
	  // If there is no money left in machine after withdrawal, close down app
	  if (this.state.currentScreen === 3 &&	this.state.total20 === 0 && 
	  this.state.total10 === 0 && this.state.total5 === 0) {
		screen = 6
		error = 2
		header = true
	  }
		
	  this.setState({
	    currentScreen: screen,
		error: error,
		headerHide: header
	  })
	  clearInterval(cardAnimation)
	}, 750)  
  }
  
  // Update balance when user enters pin successfully
  updateBalance = (balance) => {  
	let overdraft = false,
    newBalance = balance	
	
	if (this.state.balanceSet) {
	  newBalance = this.state.currentBalance
	}
	
	// If balance is already overdrawn, do not display overdraft warning
	if (balance < 0) {
      overdraft = true
	}
	  
	this.setState({
	  currentBalance: newBalance,
	  overdrawnFlag: overdraft,
	  balanceSet: true
	})
  }
  
  // Display which screen to show
  currentScreen = () => {
	switch(this.state.currentScreen) {
	  case 0:
	    return <Intro changeScreen={this.changeScreen} />
	  case 1:
	    return <PIN changeScreen={this.changeScreen} updateBalance={this.updateBalance} />
	  case 2:
	    return <Balance changeScreen={this.changeScreen} withdraw={this.overdraftCheck} balance={this.state.currentBalance} />
	  case 3:
	    return <CashDisplay changeScreen={this.changeScreen} withdrawAmm={this.state.withdrawAmm} notes={this.state.notesArray} />
	  case 4:
	    return <CustomBalance changeScreen={this.changeScreen} withdraw={this.overdraftCheck} />
      case 5:
	    return <Overdrawn changeScreen={this.changeScreen} withdraw={this.withdraw} withdrawAmm={this.state.withdrawAmm} />
      case 6:
	    return <ErrorScreen changeScreen={this.changeScreen} error={this.state.error} />
	  default:
	    return null
	}
  }

  // Check if withdrawal will cause overdraft before withdrawing
  overdraftCheck = (withdrawAmm) => {
    this.setState({
	  withdrawAmm: withdrawAmm
	})
    
	// If withdrawal is higher than will be allowed by bank rules
	if ((this.state.currentBalance - withdrawAmm) < -100) {
	  this.changeScreen(6, 1)
	}
    // If withdrawal will cause overdraft, but is still allowed	
	else if ((this.state.currentBalance - withdrawAmm) < 0 && this.state.overdrawnFlag === false) {
	  this.changeScreen(5)
	}
	// If no overdraft
	else {
	  this.withdraw(withdrawAmm)
	}
  }
  
  // Function to withdraw money and then display
  withdraw = (withdrawAmm) => {	 
	  
    let withdrawnTotal = withdrawAmm,
    counter20 = this.state.total20,
    counter10 = this.state.total10,
    counter5 = this.state.total5,
    notesArray = [],
	overdrawnAlert = false
	
	while (withdrawnTotal !== 0) {	
	  // If remaining amount is multiple of 20 etc. withdraw that note if there is one to withdraw
	  // Continues down through denominations until no notes left or full balance withdrawn
	  if((withdrawnTotal % 20) === 0 && counter20 !== 0) {		
		withdrawnTotal -= 20
		counter20--
		notesArray.push(20)
	  }
	  else if ((withdrawnTotal % 10) === 0 && counter10 !== 0) {			
		withdrawnTotal -= 10	
		counter10--
		notesArray.push(10)
	  }
	  else if ((withdrawnTotal % 5) === 0 && counter5 !== 0) {		
		withdrawnTotal -= 5
		counter5--
		notesArray.push(5)			
	  }
	  else if (counter5 === 0 && counter10 === 0 && counter20 === 0) {
		this.changeScreen(6, 0)
		break
	  }
	  else {
		this.changeScreen(6, 3)
		break
	  }
	}
	
	// If user is coming from overdraft warning, set so that warning will not display again
	if (this.state.currentScreen === 5) {
	  overdrawnAlert = true
	}
	
	// If withdrawal is completed, distribute notes and update balance, then display cash
	if (withdrawnTotal === 0) {
	  notesArray.sort(function(a, b){return b-a})
	  this.setState({
	    currentBalance: (this.state.currentBalance - withdrawAmm),
		total20: counter20,
		total10: counter10,
		total5: counter5,
		notesArray: notesArray,
		withdrawAmm: withdrawAmm,
		overdrawnFlag: overdrawnAlert
	  })	  
	  this.changeScreen(3)
	}
  }
  
  
  render() {
    return (
	  <div>
	    {this.state.headerHide ? 
		  null : <Header resetApp={this.changeScreen} />}		  
		{this.currentScreen()}
	  </div>
    )
  }
}

export default ATM;
