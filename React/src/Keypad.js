import React from 'react';
import KeypadButton from './KeypadButton';

/* Keypad for PIN/Withdraw Amount */
class Keypad extends React.Component { 

  // Draw numerical keypad up to 10 in four rows
  renderKeypad = () => {
	let keyPad = []
	let keyPadCounter = 0	
	for (let i = 0; i < 4; i++) {		
      let keyPadRow = []	  
	  for (let j = 0; j < 3; j++) {		
        keyPadCounter++		  
		if (keyPadCounter === 10) {
		  keyPadCounter = 0
		}		
	    keyPadRow.push(<KeypadButton classType="KeypadButton-keypad" key={keyPadCounter} buttonLabel={keyPadCounter} buttonValue={keyPadCounter} 
		buttonClick={this.props.keypadClick} />)	
		if (keyPadCounter === 0) {
		  break
		}
	  }	  
	  keyPad.push(<div key={i} className="board-row">{keyPadRow}</div>)
	}
	return keyPad
  }
  
  render() {
    return(
	  <div>
	    {this.renderKeypad()}
	  </div>
	)
  }
}

export default Keypad;