import React from 'react';
import './CustomBalance.css';
import Keypad from './Keypad';

/* Screen which lets user enter custom withdrawal amount */
class CustomBalance extends React.Component { 

  constructor() {
    super();
    this.state = {
	  balanceEntry: "",
	  fadeTransition: 0
	};
  }
  
  // When component mounts, fade in
  componentDidMount() {	
	let fadeIn = setInterval(() => {
	  this.setState({
	    fadeTransition: 1
	  })
	  clearInterval(fadeIn)
	}, 10)
  }
  
  // If user clicks keypad, add value to user entry
  keypadClick = (i) => {  
    this.setState({
      balanceEntry: this.state.balanceEntry + i
	})
  }

  
  // Resets user entry
  clearEntry = () => {
    this.setState({
      balanceEntry: ""
	})
  }  
    
  render() {  
    return (
      <div className="mainContainer" style={{'opacity' : this.state.fadeTransition}}>
        <p>
		  Enter how much you'd like to withdraw
		</p>
		<div className="CustomBalance-entry">
		  £{this.state.balanceEntry}
		</div>
		<br />
        <div>
		  <Keypad keypadClick={this.keypadClick} />
		</div>
		<table>
		  <tbody>
		    <tr>
			  <td>
		        <button className="button" onClick={this.clearEntry}>
		          Clear
		        </button>
			  </td>
			</tr>
			<tr>
			  <td>
		        <button className="button" onClick={() => this.props.withdraw(this.state.balanceEntry)}>
		          Submit
		        </button>
			  </td>
			</tr>
	      </tbody>
		</table>
      </div>
    );
  }
}

export default CustomBalance;